# WLED-Shield

This is a simple shield for [WLED](https://kno.wled.he). The shield is intended for NodeMCU-ESP32S or
pin compatible boards.

Features:
* Support for up to 4 independent LED strips
* Supports 5V and 12V LED strips
* Power Off via Mosfet for all strips to reduce standy by energy usage
* Resettable 10A PTC fuse
* Screw terminals to support a wide array of power supplies and LED strips

Pinout:

| Description | GPIO |
|-------------|------|
| Mosfet/Relay | GPIO12/P12 |
| LED 1 Data | GPIO2/P2 |
| LED 1 Clock | GPIO0/P0 |
| LED 2 Data | GPIO4/P4 |
| LED 2 Clock |  GPIO16/P16 |
| LED 3 Data | GPIO17/P17 |
| LED 3 Clock | GPIO5/P5 |
| LED 4 Data | GPIO18/P18 |
| LED 4 Clock | GPIO19/P19 |
